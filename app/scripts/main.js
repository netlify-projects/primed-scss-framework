'use strict';

$(document).ready(function () {


  //----------------------------------------------------------------------------
  // Variables
  //----------------------------------------------------------------------------


  const menuButton            = $('#menu-button');
  const navList       = $('nav ul');


  //----------------------------------------------------------------------------
  // Functions
  //----------------------------------------------------------------------------

  function forceExpandNavbar() {
    if (menuButton.hasClass('menu-open')) {
      menuButton.removeClass('menu-open');
    }
    if (navList.hasClass('collapsed-visible')) {
      navList.removeClass('collapsed-visible');
    }
  }

  function togglenavList() {
    navList.toggleClass('collapsed-visible');
  }

  function toggleMenuButtonState() {
    menuButton.toggleClass('menu-open');
  }

  function menuButtonToggleFunction() {
    toggleMenuButtonState();
    togglenavList();
  }

  function resizeAction() {
    console.log();
    if ($(window).width > 767) {
      forceExpandNavbar();
      console.log('forece closing menu');
    }
  }

  menuButton.click(menuButtonToggleFunction);

  $(window).on('resize', function() {
    let currentWidth = $(window).width();
    // console.log('width: ' + currentWidth);
    if (currentWidth > 767) {
      // console.log('too wide!');
      forceExpandNavbar();
    }
  });
});
